import utils
import configparser
from datetime import datetime

from model.Production import Production


# Loading parameters
config = configparser.ConfigParser()
parameters = config.read("parameters.ini")

# Loading logs
print("Loading logs...")
logs_path = config["general"]["logs_path"]
logs_extension = config["general"]["extension"]
logs = utils.load_logs(logs_path, logs_extension)
print("Loaded " + str(len(logs)) + " logs")

# Sorting logs in temporal order
print("Sorting logs...")
sorted_logs = utils.sort_logs(logs)

# Splitting the logs into productions
print("Splitting productions...")
productions = utils.split_productions(logs, config)
print("Detected " + str(len(productions)) + " productions")

# Creating the csv file with the productions details
print("Creating csv...")
utils.create_csv(productions, config)

# Filtering productions by dates
date_format = "%d/%m/%Y %H:%M:%S"
start_date = datetime.strptime(config["filters"]["start_date"], date_format)
end_date = datetime.strptime(config["filters"]["end_date"], date_format)
filtered_productions = utils.filter_by_dates(productions, start_date, end_date)
# Printing of found productions in time range
print("Productions found in time range: " + str(len(filtered_productions)))
if len(filtered_productions) == 0:
    print("no productions found in the time span selected")
    exit(1)

# Drawing productions bur histogram
die_diameter = float(config["details"]["die_diameter"])
die_gap = float(config["details"]["die_gap"])
plot_productions_distributions = config.getboolean("plots", "productions_distributions")
if plot_productions_distributions:
    utils.plot_distributions(filtered_productions, die_diameter, die_gap)

# Filtering productions by BUR
bur_min = float(config["filters"]["bur_min"])
bur_max = float(config["filters"]["bur_max"])
filtered_productions = utils.filter_by_bur(filtered_productions, bur_min, bur_max, die_diameter)
# Printing of found productions with given bur
print("Productions found in bur range: " + str(len(filtered_productions)))

# Filtering productions by hauloff speed
speed_min = float(config["filters"]["speed_min"])
speed_max = float(config["filters"]["speed_max"])
filtered_productions = utils.filter_by_speed(filtered_productions, speed_min, speed_max)
# Printing of found productions with given speed
print("Productions found in speed range: " + str(len(filtered_productions)))

# Filtering productions by thickness
thick_min = float(config["filters"]["thick_min"])
thick_max = float(config["filters"]["thick_max"])
filtered_productions = utils.filter_by_thick(filtered_productions, thick_min, thick_max)
# Printing of found productions with given thickness
print("Productions found in thickness range: " + str(len(filtered_productions)))


# Filtering the productions on length
min_length = int(config["filters"]["min_length"])
filtered_productions = utils.filter_by_length(filtered_productions, min_length)
print("Productions with minimum length: " + str(len(filtered_productions)))

# If enabled, filtering the productions that don't start with flat actuators
require_starting_flat = config.getboolean("filters", "require_starting_flat")
if require_starting_flat:
    filtered_productions = utils.filter_by_starting_flat(filtered_productions)
    print("Productions starting flat: " + str(len(filtered_productions)))

# Splitting productions in 2 groups based on actuation status
filtered_productions_active = utils.filter_by_actuation_active(filtered_productions, True)
print("Productions with regulation active: " + str(len(filtered_productions_active)))
# for fp in filtered_productions_active:
#     print("ACTIVE Prod. START: ", fp.get_start_date())
#     print("ACTIVE Prod. #LOG : ", str(len(fp.get_logs())))
filtered_productions_inactive = utils.filter_by_actuation_active(filtered_productions, False)
print("Productions with regulation inactive: " + str(len(filtered_productions_inactive)))
# for fp in filtered_productions_inactive:
#     print("INACTIVE Prod. START: ", fp.get_start_date())
#     print("INACTIVE Prod. #LOG : ", str(len(fp.get_logs())))

# Computing the statistics
statistics_mixed = utils.calculate_statistics(filtered_productions, config)
statistics_active = utils.calculate_statistics(filtered_productions_active, config)
statistics_inactive = utils.calculate_statistics(filtered_productions_inactive, config)

# Printing the results
title = ""
statistics = None
for i in range(3):
    if i == 0:
        statistics = statistics_mixed
        title = "MIXED"
    elif i == 1:
        statistics = statistics_inactive
        title = "INACTIVE"
    elif i == 2:
        statistics = statistics_active
        title = "ACTIVE"

    # Computing the avg sigma improvement
    if statistics['2sigma_start'] != 0:
        sigma_improvement = \
            round((statistics['2sigma_start'] - statistics['2sigma_avg']) / statistics['2sigma_start'] * 100, 2)
    else:
        sigma_improvement = 0.0

    # Computing BUR
    bur = Production.compute_bur(statistics['layflat_avg'], die_diameter)

    print("\n### STATISTICS " + title + " ###")
    print(f"thickness: {statistics['thickness_avg']} um\n"
              f"layflat: {statistics['layflat_avg']} mm\n"
              f"speed: {statistics['speed_avg']} m/min\n"
              f"BUR: {bur}\n"
              f"2sigma start: {statistics['2sigma_start']}%\n"
              f"2sigma avg: {statistics['2sigma_avg']}%\n"
              f"2sigma best: {statistics['2sigma_best']}%\n"
              f"2sigma worst: {statistics['2sigma_worst']}%\n"
              f"2sigma impr. avg: {sigma_improvement}%\n"
              f"2sigma of 2sigmas: {statistics['2sigma_of_2sigmas']}%")

# Plotting the improvements distribution
plot_improvements_distributions = config.getboolean("plots", "improvements_distributions")
if not len(filtered_productions_inactive) == 0 and plot_improvements_distributions:
    utils.plot_improvements_distribution("Productions with regulation inactive", filtered_productions_inactive)

plot_actuation_sigma_trends = config.getboolean("plots", "actuation_sigma_trends")
if not len(filtered_productions_active) == 0:
    if plot_improvements_distributions:
        utils.plot_improvements_distribution("Productions with regulation active", filtered_productions_active)
    if plot_actuation_sigma_trends:
        utils.compute_actuators_incidence(filtered_productions_active)
