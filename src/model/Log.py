import datetime

from model.log.Recipe import Recipe
from model.log.Actuation import Actuation
from model.log.Stats import Stats


class Log:

    def __init__(self, title: str, jsonobject):
        self.title = title
        self.parameters = jsonobject["parameters"]
        self.recipe = Recipe(jsonobject)
        self.application = jsonobject["status"]["application"]
        self.measurer = jsonobject["status"]["measurer"]
        self.hauloff = jsonobject["status"]["hauloff"]
        self.stats = Stats(jsonobject)
        self.profile_raw = jsonobject["rawProfile"]
        self.profile_zones = jsonobject['zonesProfile']
        self.actuation = Actuation(jsonobject)

        # Extracting the timestamp
        creation_time = float(title.split("/")[-1].split("-")[-1].replace('.json', '')) / 1000
        self.timestamp = datetime.datetime.fromtimestamp(creation_time)

    def get_title(self) -> str:
        return self.title

    def get_timestamp(self) -> datetime:
        return self.timestamp

    def get_parameters(self):
        return self.parameters

    def get_recipe(self) -> Recipe:
        return self.recipe

    def get_application(self):
        return self.application

    def get_measurer(self):
        return self.measurer

    def get_haoulff(self):
        return self.hauloff

    def get_stats(self) -> Stats:
        return self.stats

    def get_raw_profile(self):
        return self.profile_raw

    def get_zones_profile(self):
        return self.profile_zones

    def get_actuation(self):
        return self.actuation
