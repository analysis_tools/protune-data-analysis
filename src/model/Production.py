import math
import datetime
import numpy as np
from typing import List

from model.Log import Log


class Production:

    def __init__(self, trigger: str, logs: List[Log], start_date: datetime, stop_date: datetime, sigma_start: float,
                 starts_flat: bool):
        self.trigger = trigger
        self.logs = logs
        self.start_date = start_date
        self.stop_date = stop_date
        self.sigma_start = sigma_start
        self.starts_flat = starts_flat
        # Initializing averages values
        self.sigma_perc_avg = None
        self.thickness_avg = None
        self.layflat_avg = None
        self.flowrate_avg = None
        self.speed_avg = None
        # Initializing utility variables
        self.actuation_variance_trend = None

    def get_trigger(self) -> str:
        return self.trigger

    def get_logs(self) -> List[Log]:
        return self.logs

    def get_start_date(self) -> datetime:
        return self.start_date

    def get_stop_date(self) -> datetime:
        return self.stop_date

    def get_starts_flat(self) -> bool:
        return self.starts_flat

    def get_sigma_start(self) -> float:
        return self.sigma_start

    def get_sigma_perc_avg(self) -> float:
        if self.sigma_perc_avg is None:
            sigma_sum = 0
            for log in self.logs:
                sigma_perc = log.get_stats().get_sigma_perc()
                if not math.isnan(sigma_perc):
                    sigma_sum += sigma_perc
            self.sigma_perc_avg = sigma_sum / len(self.logs)
        return self.sigma_perc_avg

    def get_thickness_avg(self):
        if self.thickness_avg is None:
            thickness_sum = 0
            for log in self.logs:
                thickness_sum += log.get_recipe().get_thickness()
            self.thickness_avg = thickness_sum / len(self.logs)
        return self.thickness_avg

    def get_layflat_avg(self):
        if self.layflat_avg is None:
            layflat_sum = 0
            for log in self.logs:
                layflat_sum += log.get_recipe().get_layflat()
            self.layflat_avg = layflat_sum / len(self.logs)
        return self.layflat_avg

    def get_flowrate_avg(self):
        if self.flowrate_avg is None:
            flowrate_sum = 0
            for log in self.logs:
                flowrate_sum += log.get_recipe().get_flowrate()
            self.flowrate_avg = flowrate_sum / len(self.logs)
        return self.flowrate_avg

    def get_speed_avg(self):
        if self.speed_avg is None:
            speed_sum = 0
            for log in self.logs:
                speed_sum += log.get_recipe().get_speed()
            self.speed_avg = speed_sum / len(self.logs)
        return self.speed_avg

    def get_actuation_variance_trend(self):
        if self.actuation_variance_trend is None:
            var_trend = []
            for log in self.logs:
                var_trend.append(np.var(log.get_actuation().actuators))
            self.actuation_variance_trend = var_trend
        return self.actuation_variance_trend

    def started_flat(self) -> bool:
        actuators = self.get_logs()[0].get_actuation().get_old_actuators()
        first_value = actuators[0]
        for value in actuators:
            if first_value != value:
                return False
        return True

    def get_sigma_after(self, logs_number) -> float:
        counter = 0
        for log in self.logs:
            if log.get_measurer()['is_valid']:
                counter += 1
                if counter == logs_number:
                    return log.get_stats().sigma_perc

        return -1

    def get_time_to_halve_sigma(self) -> int:
        start_log = None
        for log in self.logs:
            if log.get_stats().get_sigma_perc() == self.get_sigma_start() and log.get_measurer()['is_valid']:
                start_log = log
                break

        halving_timestamp = None
        for log in self.logs:
            if log.get_measurer()['is_valid'] and start_log.get_stats().get_sigma_perc() / 2 > log.get_stats().get_sigma_perc():
                halving_timestamp = log.get_timestamp()
                break

        if halving_timestamp is None:
            return -1

        return (halving_timestamp - start_log.get_timestamp()).total_seconds() // 60

    @staticmethod
    def compute_bur(layflat: float, die_diameter: float) -> float:
        return round((layflat * 2) / (math.pi * die_diameter), 2)
