class Actuation:

    def __init__(self, jsonobject):
        self.active = jsonobject["status"]["application"]["actuation_active"]
        self.actuators = jsonobject["actuators"]["values"]
        self.forced = jsonobject["actuators"]["forced"]
        self.old_actuators = jsonobject['actuators']["values"]
        self.old_forced = jsonobject['actuators']["forced"]

    def is_active(self) -> bool:
        return self.active

    def get_actuators(self):
        return self.actuators

    def get_forced(self):
        return self.forced

    def get_old_actuators(self):
        return self.old_actuators

    def get_old_forced(self):
        return self.old_forced

    def are_actuators_equal(self, other):
        # If the actuators length is different
        if len(self.actuators) != len(other.get_actuators()):
            return False
        # Checking the values of the single actuators
        for index in range(len(self.actuators)):
            if self.actuators[index] != other.get_actuators()[index]:
                return False
        return True

    def are_actuators_flat(self):
        for value in self.actuators:
            if self.actuators[0] != value:
                return False
        return True

    def are_actuators_flat_with_alignment_forcing(self):
        # Searching 1st not maxed actuator
        flat_value = -1
        for value in self.actuators:
            if value != 100:
                flat_value = value
                break

        maxed_actuators = 0
        for value in self.actuators:
            if value == 100:
                maxed_actuators += 1
                # If more than 3 actuators are maxed, it's not an autoalignment forcing phase
                if maxed_actuators > 3:
                    return False
            # If a value different than the 1st actuator exists, it's not a flat autoalignment forcing phase
            elif value != flat_value:
                return False

        # If less than 3 actuators are maxed, it's not an autoalignment forcing phase
        if maxed_actuators != 3:
            return False

        return True
