class Recipe:

    def __init__(self, jsonobject):
        self.thickness = float(jsonobject["production"]["thickness"])
        self.layflat = float(jsonobject["production"]["layflat"])
        self.speed = float(jsonobject["production"]["line_speed"])
        self.flowrate = float(jsonobject["production"]["flowrate"])

    def get_thickness(self) -> float:
        return self.thickness

    def get_layflat(self) -> float:
        return self.layflat

    def get_speed(self) -> float:
        return self.speed

    def get_flowrate(self) -> float:
        return self.flowrate

    def __eq__(self, other):
        return self.thickness == other.thickness and \
               self.layflat == other.layflat and \
               self.speed == other.speed

    def __ne__(self, other):
        return self.thickness != other.thickness or \
               self.layflat != other.layflat or \
               self.speed != other.speed
