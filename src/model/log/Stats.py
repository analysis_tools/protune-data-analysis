import configparser
import numpy as np


class Stats:

    def __init__(self, jsonobject):
        if len(jsonobject["stats"]) != 2:
            self.min_thickness = float(jsonobject["stats"]["min_thickness"])
            self.max_thickness = float(jsonobject["stats"]["max_thickness"])
            self.sigma_perc = float(jsonobject["stats"]["sigma_perc"])
            self.sigma_abs = float(jsonobject["stats"]["sigma_abs"])
            self.std_dev = float(jsonobject["stats"]["stdDev"])
        else:
            self.min_thickness = float(jsonobject["stats"]["statsProfileZone"]["min_thickness"])
            self.max_thickness = float(jsonobject["stats"]["statsProfileZone"]["max_thickness"])
            self.sigma_perc = float(jsonobject["stats"]["statsProfileZone"]["sigma_perc"])
            self.sigma_abs = float(jsonobject["stats"]["statsProfileZone"]["sigma_abs"])
            self.std_dev = float(jsonobject["stats"]["statsProfileZone"]["stdDev"])

        # Substituting given stats with stats computed on raw profile
        config = configparser.ConfigParser()
        config.read("parameters.ini")
        use_raw_profile = config.getboolean("analysis", "use_raw_profile")
        if use_raw_profile:
            #print("using raw profile")
            profile = jsonobject["rawProfile"]
        else:
            profile = jsonobject["zonesProfile"]
        self.std_dev = np.std(profile)
        self.sigma_abs = 2 * self.std_dev
        if profile is not None:
            self.sigma_perc = self.sigma_abs / np.mean(profile) * 100

        actuators = jsonobject["actuators"]["values"]
        if actuators is not None:
            self.actuators_std_dev = np.std(actuators)
            self.actuators_mean = np.mean(actuators)

    def get_min_thickness(self) -> float:
        return self.min_thickness

    def get_max_thickness(self) -> float:
        return self.max_thickness

    def get_sigma_perc(self) -> float:
        return self.sigma_perc

    def get_sigma_abs(self) -> float:
        return self.sigma_abs

    def get_std_dev(self) -> float:
        return self.std_dev

    def get_actuators_mean(self) -> float:
        return self.actuators_mean

    def get_actuators_std_dev(self):
        return self.actuators_std_dev

