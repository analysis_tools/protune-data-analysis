import os
import math
import json
import datetime
import configparser
import numpy as np
import matplotlib.pyplot as plt
from typing import List
import csv

from model.Log import Log
from model.Production import Production


# Function that loads the logs, with the desired extension, from a given path
def load_logs(path: str, extension: str) -> List[Log]:
    # Loading files in path
    files = os.listdir(path)
    # Loading logs
    logs = []
    skipped_files = 0
    for log_name in files:
        if log_name.endswith(extension):
            log_file = open(path + "/" + log_name, "r")
            try:
                log_json = json.load(log_file)
                logs.append(Log(log_file.name, log_json))
                log_file.close()
            except UnicodeDecodeError:
                #print("\n" + log_name + " malformed, skipping")
                skipped_files += 1
            except json.decoder.JSONDecodeError:
                #print("\n" + log_name + " malformed, skipping")
                skipped_files += 1
        print_progress_bar(len(logs), len(files), prefix="Loading", suffix="completed", length=50, print_end="")
    print("\nSkipped " + str(skipped_files) + " malformed files")
    return logs


# Function that sorts the logs in ascending temporal order
def sort_logs(logs: List[Log]):
    return sorted(logs, key=lambda log: log.get_timestamp())


# Function that splits the productions based on the following triggers:
# - Recipe changed (with tolerance)
# - Actuation started/stopped
# - Actuators changed manually while regulation stopped
# - Too much time elapsed between consecutive profiles
def split_productions(logs: List[Log], config: configparser):
    # Parameters retrieval
    thickness_tolerance = float(config["splitting"]["thickness_tolerance"])
    layflat_tolerance = float(config["splitting"]["layflat_tolerance"])
    speed_tolerance = float(config["splitting"]["speed_tolerance"])
    max_time_between_profiles = float(config["filters"]["max_time_between_profiles"])
    profiles_for_sigma_start = int(config["analysis"]["profiles_for_sigma_start"])

    # Lists
    productions = []
    current_production_logs = []

    # Algorithm variables
    start_date = None
    end_date = None
    split_production = False
    current_trigger = ""
    next_trigger = ""
    discarded = 0
    discarded2 = 0
    thrown = 0
    previous_split_index = 0
    starts_flat = False

    for i in range(len(logs)):

        # Saving logs for convenience
        current = logs[i]
        if i > 0:
            previous = logs[i - 1]
        else:
            previous = current

        # Checking the conditions to split the productions
        if i == 0:
            current_trigger = "none"

        try:
            is_valid = current.get_measurer()['is_valid']
            if not is_valid:
                # print("Discarded because measurer not valid, profile: " + str(current.get_timestamp()))
                discarded += 1
        except KeyError:
            is_valid = previous.get_haoulff()['direction'] == current.get_haoulff()['direction']
            if not is_valid:
                # print("Discarded because an hauloff inversion took place, profile: " + str(current.get_timestamp()))
                discarded2 += 1

        # qui non metto throw_profile perché non mi serve nel determinare le produzioni
        if current.get_recipe() != previous.get_recipe():
            if abs(current.get_recipe().get_thickness() - previous.get_recipe().get_thickness()) > \
                    previous.get_recipe().get_thickness() * thickness_tolerance / 100 or \
                    abs(current.get_recipe().get_layflat() - previous.get_recipe().get_layflat()) > \
                    previous.get_recipe().get_layflat() * layflat_tolerance / 100 or \
                    abs(current.get_recipe().get_speed() - previous.get_recipe().get_speed()) > \
                    previous.get_recipe().get_speed() * speed_tolerance / 100:
                split_production = True
                next_trigger += " - production details changed"
        if current.get_actuation().is_active() != previous.get_actuation().is_active():
            if current.get_actuation().is_active():
                next_trigger += " - started regulation"
            else:
                next_trigger += " - stopped regulation"
            split_production = True
        if not current.get_actuation().is_active() and \
                not current.get_actuation().are_actuators_equal(previous.get_actuation()):
            next_trigger += " - actuators changed manually"
            split_production = True
        if current.get_actuation().is_active() and \
                current.get_actuation().are_actuators_flat() and \
                not previous.get_actuation().are_actuators_flat_with_alignment_forcing() and \
                not previous.get_actuation().are_actuators_flat():
            next_trigger += " - actuation reset"
            split_production = True
        if (current.get_timestamp() - previous.get_timestamp()).total_seconds() // 3600 > max_time_between_profiles:
            next_trigger += " - too much time between consecutive profiles"
            split_production = True

        if split_production:
            # Checking if the current production log list is empty
            sigma_start = 0
            for j in range(len(current_production_logs)):
                if current_production_logs[j].get_measurer()['is_valid']:
                    sigma_start = current_production_logs[j].get_stats().get_sigma_perc()
                    break
            # if sigma_start == 0:
            #    sigma_start = np.std(current_production_logs[0].get_raw_profile())
            productions.append(
                Production(current_trigger.lstrip(" - "), current_production_logs, start_date, end_date, sigma_start,
                           starts_flat))
            current_trigger = next_trigger
            previous_split_index = i
            starts_flat = np.count_nonzero(np.array(previous.get_actuation().actuators) ==
                                           previous.get_actuation().actuators[0]) \
                          == len(previous.get_actuation().actuators)
            # Resetting the variables
            current_production_logs = []
            start_date = None
            next_trigger = ""
            split_production = False

        # Adding the current log to the list
        current_production_logs.append(current)

        if throw_profile(current, config, False):
            thrown += 1

        # Updating the dates
        if start_date is None:
            start_date = current.get_timestamp()

    # Adding last production
    if len(current_production_logs) > 0:
        sigma_start = 0
        for j in range(len(current_production_logs)):
            if current_production_logs[j].get_measurer()['is_valid']:
                sigma_start = current_production_logs[j].get_stats().get_sigma_perc()
                break
        productions.append(
            Production(current_trigger.lstrip(" - "), current_production_logs, start_date, end_date, sigma_start,
                       starts_flat))

    total_profiles = 0
    for production in productions:
        total_profiles += len(production.get_logs())

    print("invalid profiles: ", discarded)
    print("inversion profiles: ", discarded2)
    print("thrown: ", thrown)
    print("logs: ", len(logs))
    print("total profiles: ", total_profiles)

    return productions


# Function that computes the starting 2sigma of a production
def compute_sigma_start(logs: List[Log], index: int, sigma_to_average: int, config: configparser) -> float:
    i = 0
    counter = 0
    sigma_start = 0

    while counter < sigma_to_average and index - i >= 0:
        log = logs[index - i]

        try:
            is_valid = log.get_measurer()['is_valid']
        except KeyError:
            is_valid = log.get_haoulff()['direction'] == log.get_haoulff()['direction']

        # qui non metto throw_profile perché ormai i profili sono già stati selezionati
        if is_valid:
            rotation_time = 36
            if i > 0:
                try:
                    speed = logs[index - i + 1].get_measurer()['speed']
                except KeyError:
                    speed = -1

                if speed <= 0:
                    rotation_time = 210
                elif 0 < speed < 2:
                    rotation_time = 180
                elif 2 <= speed < 4:
                    rotation_time = 120
                elif 4 <= speed < 6:
                    rotation_time = 60

            if i == 0 or (logs[index - i + 1].get_timestamp() - log.get_timestamp()).total_seconds() \
                    < rotation_time * 2:

                sigma_start += log.get_stats().get_sigma_perc()
                counter += 1
            else:
                break
        i += 1
    if counter > 0:
        sigma_start /= counter

    return sigma_start


# Function that filters productions by dates
def filter_by_dates(productions: List[Production], start_date: datetime, end_date: datetime):
    filtered_productions = []
    for production in productions:
        if start_date <= production.get_start_date() < end_date:
            filtered_productions.append(production)
            # print("Production start date:" + str(production.get_start_date()))
        # else:
        #    print("NOT IN RANGE Production start date:" + str(production.get_start_date()))

    return filtered_productions


# Function that filters productions by bur
def filter_by_bur(productions: List[Production], bur_min: float, bur_max: float, die_diameter: float):
    filtered_productions = []
    for production in productions:
        bur = Production.compute_bur(production.get_layflat_avg(), die_diameter)
        if bur_min <= bur <= bur_max:
            filtered_productions.append(production)
    return filtered_productions


# Function that filters productions by hauloff speed
def filter_by_speed(productions: List[Production], speed_min: float, speed_max: float):
    filtered_productions = []
    for production in productions:
        if speed_min <= production.get_speed_avg() <= speed_max:
            filtered_productions.append(production)
    return filtered_productions


# Function that filters productions by thickness
def filter_by_thick(productions: List[Production], thick_min: float, thick_max: float):
    filtered_productions = []
    for production in productions:
        if thick_min <= production.get_thickness_avg() <= thick_max:
            filtered_productions.append(production)
    return filtered_productions


# Function that filters the productions based on the production length
def filter_by_length(productions: List[Production], min_length):
    filtered_productions = []
    for production in productions:
        if len(production.get_logs()) >= min_length:
            filtered_productions.append(production)
    return filtered_productions


# Function that filter the productions that do not have the regulation
# active
def filter_by_actuation_active(productions: List[Production], active: bool):
    filtered_productions = []
    for production in productions:
        if production.get_logs()[0].get_actuation().is_active() == active:
            filtered_productions.append(production)
    return filtered_productions


# Function that filter the productions that do not start the regulation
# with flat actuators
def filter_by_starting_flat(productions: List[Production]):
    filtered_productions = []
    for production in productions:
        if production.started_flat():
            filtered_productions.append(production)
    return filtered_productions


# Function that filters the profiles that were not read right
def throw_profile(log: Log, config: configparser, stampa: bool):
    profile = log.get_raw_profile()
    count_zeros = 0
    two_sigma_max = float(config["filters"]["2sigma_max"])
    if profile is not None:
        sp = (np.std(profile) / np.mean(profile)) * 100

    for value in profile:
        if value == 0:
            count_zeros += 1

    if stampa:
        print("PROFILE è maggiore di" + str(two_sigma_max) + ": " + str(sp))
        print(log.get_title())

    return (count_zeros > 5) or (2 * sp > two_sigma_max)


# Function that computes the statistics values of a set of productions
def calculate_statistics(productions: List[Production], config: configparser):
    results = {}
    # Calculating statistics of productions
    total_length = 0
    sigma_start = 0
    sigma_best = 0
    sigma_worst = 0
    sigma_average = 0
    thickness = 0
    layflat = 0
    speed = 0

    # Summing the values of the different productions
    for production in productions:
        # Recipe values
        thickness += production.get_thickness_avg()
        layflat += production.get_layflat_avg()
        speed += production.get_speed_avg()
        # Outliers filter (if enabled)
        sigmas = []
        for log in production.get_logs():
            sigmas.append(log.get_stats().get_sigma_perc())
        if config.getboolean("filters", "discard_outliers"):
            outliers_k = float(config["filters"]["outliers_multiplier"])
            sigmas = remove_outliers(sigmas, outliers_k)

        # Two sigmas are weighted on productions length
        current_production_length = len(sigmas)
        if current_production_length > 0:
            total_length += current_production_length
            sigma_start += production.get_sigma_start() * current_production_length
            sigma_best += np.min(sigmas) * current_production_length
            sigma_worst += np.max(sigmas) * current_production_length
            if sigmas is not None:
                sigma_average += np.mean(sigmas) * current_production_length

    # Dividing recipe values for number of productions to find avgs
    if not len(productions) == 0:
        thickness /= len(productions)
        layflat /= len(productions)
        speed /= len(productions)
    # Dividing sigma values for numer of total logs (sigmas are weighted on productions length)
    if not total_length == 0:
        sigma_start /= total_length
        sigma_best /= total_length
        sigma_worst /= total_length
        sigma_average /= total_length

    # Computing 2sigma of 2sigmas
    sigma_of_sigmas = 0
    for production in productions:
        sigma_of_sigmas += math.pow((sigma_average - production.get_sigma_perc_avg()) / sigma_average * 100, 2)

    if not len(productions) == 0:
        sigma_of_sigmas = math.sqrt(sigma_of_sigmas / len(productions))

    # Populating the result dictionary
    results["thickness_avg"] = round(thickness, 2)
    results["layflat_avg"] = round(layflat, 2)
    results["speed_avg"] = round(speed, 2)
    results["2sigma_start"] = round(sigma_start, 2)
    results["2sigma_avg"] = round(sigma_average, 2)
    results["2sigma_best"] = round(sigma_best, 2)
    results["2sigma_worst"] = round(sigma_worst, 2)
    results["2sigma_of_2sigmas"] = round(sigma_of_sigmas, 2)

    return results


# Function that extracts the median of a set of data
def find_median(data: List[float], start: int, end: int):
    middle = ((end - start) // 2) + start
    if (end - start) % 2 == 1:
        return data[middle]
    else:
        return (data[middle - 1] + data[middle]) / 2


# Function that removes outliers from a set of data
def remove_outliers(data: List[float], k: float) -> List[float]:
    if len(data) > 10:
        new_data = [x for x in data if not np.isnan(x)]
        sorted_data = sorted(new_data)
        data_median = np.median(sorted_data)  # .astype(int)
        upper_median = [x for x in sorted_data if x > data_median]
        bottom_median = [x for x in sorted_data if x < data_median]
        # q1 = np.median(sorted_data[:data_median])
        # q3 = np.median(sorted_data[data_median:])
        q1 = np.median(bottom_median)
        q3 = np.median(upper_median)
        min_range = q1 - k * (q3 - q1)
        max_range = q3 + k * (q3 - q1)

        filtered_data = []
        outliers = []
        for value in new_data:
            if min_range < value < max_range:
                filtered_data.append(value)
            else:
                outliers.append(value)
    else:
        return data

    # print("filtered " + str(len(sorted_data) - len(filtered_data)) + "/" + str(len(sorted_data)) + " outliers")
    return filtered_data


# Function that computes the burs histogram
def plot_distributions(productions: List[Production], die_diameter: float, die_gap: float):
    burs = []
    speeds = []
    mds = []
    tds = []
    thicks = []

    for production in productions:
        bur = Production.compute_bur(production.get_layflat_avg(), die_diameter)
        md = (die_diameter * die_gap * np.pi) / (production.get_layflat_avg() * production.get_thickness_avg()
                                                 * 2 * 0.001)
        burs.append(bur)
        speeds.append(production.get_speed_avg())
        mds.append(md)
        tds.append(md / bur)
        thicks.append(production.get_thickness_avg())

    fig, axs = plt.subplots(2, 3)
    # BURs distribution
    axs[0, 0].hist(x=burs, bins=np.arange(np.floor(np.min(burs)), np.ceil(np.max(burs)), 0.1))
    axs[0, 0].title.set_text("BURs distribution")
    axs[0, 0].xaxis.set_ticks(np.arange(np.floor(np.min(burs)), np.ceil(np.max(burs)), 0.1))
    axs[0, 0].grid()
    # THICKs distribution
    axs[1, 0].hist(x=thicks, bins=np.arange(np.floor(np.min(thicks)), np.ceil(np.max(thicks)), 1))
    axs[1, 0].title.set_text("THICKs distribution")
    axs[1, 0].xaxis.set_ticks(np.arange(np.floor(np.min(thicks)), np.ceil(np.max(thicks)), 1))
    axs[1, 0].grid()
    # Speeds distribution
    axs[0, 1].hist(x=speeds, bins=np.arange(np.floor(np.min(speeds)) - 2, np.ceil(np.max(speeds)) + 3, 1))
    axs[0, 1].title.set_text("SPEEDs distribution")
    axs[0, 1].grid()
    # MD distribution
    axs[1, 1].hist(x=mds, bins=np.arange(np.floor(np.min(mds)), np.ceil(np.max(mds)), 1))
    axs[1, 1].title.set_text("MDs distribution")
    axs[1, 1].grid()
    # MD/TD distribution
    axs[1, 2].hist(x=tds, bins=np.arange(np.floor(np.min(tds)), np.ceil(np.max(tds)), 1))
    axs[1, 2].title.set_text("MD/TDs distribution")
    axs[1, 2].grid()
    fig.text(0.5, 0.04, 'Distribution', ha='center', va='center')
    fig.text(0.06, 0.5, 'Frequency', ha='center', va='center', rotation='vertical')
    plt.show()


# Function that draws the distribution of the 2sigma improvements
def plot_improvements_distribution(title: str, productions: List[Production]):
    # Computing the average 2sigma improvement for each production
    improvements = []
    for production in productions:
        sigma_start = production.get_sigma_start()
        sigma_pa = production.get_sigma_perc_avg()
        if sigma_start > 0:
            improvement = (sigma_start - sigma_pa) / sigma_start * 100
        else:
            improvement = 0
        if not math.isnan(improvement):
            improvements.append(improvement)

    plt.hist(x=improvements, bins=np.arange(np.floor(np.min(improvements)) - 1, np.ceil(np.max(improvements)) + 1, 1))
    plt.grid()
    plt.xlabel("2-Sigma improvement (%)")
    plt.ylabel("# of productions")
    plt.title(title)
    plt.show()


def create_csv(productions: List[Production], config: configparser):
    outliers_removed = 0
    prev_len_sigmas = 0
    current_production_length_tot = 0
    production_length = 0

    log_for_actuation_stats = 15
    if config.get("actuators", "regulations"):
        log_for_actuation_stats = int(config["actuators"]["regulations"])

    sigma_improvement_after_string = "2sigma improvement after " + str(log_for_actuation_stats) + " profiles"
    actuation_mean_string = "actuation mean after " + str(log_for_actuation_stats) + " profiles"
    actuation_std_dev_string = "actuation std dev after " + str(log_for_actuation_stats) + " profiles"
    zeroed_actuators_string = "zeroed actuators after " + str(log_for_actuation_stats) + " profiles"
    # Init csv writer
    with open(config["general"]["logs_path"] + '\\' + str(
            round(datetime.datetime.now().timestamp())) + '_log_produzioni.csv', mode='w', newline='') as csv_file:
        fieldnames = ['date', 'time', 'cause', 'actuation started flat', 'actuation active', 'profiles',
                      'outliers', 'total length', 'outliers percentage', 'zones',
                      'layflat', 'flowrate', 'BUR', 'speed m/min', 'thickness um',
                      '2sigma start', '2sigma mean', '2sigma best', '2sigma worst',
                      '2sigmaW - 2sigmaB', '2sigma improvement', 'time to halve 2sigma', sigma_improvement_after_string,
                      '2sigma under average', '2sigma sigma', actuation_mean_string, actuation_std_dev_string,
                      zeroed_actuators_string]

        writer = csv.DictWriter(csv_file, dialect='excel', fieldnames=fieldnames)
        writer.writeheader()
        prod_count = 0
        for production in productions:
            sigma_sigmas = 0
            sigma_counter = 0

            sigmas = []
            for log in production.get_logs():
                if log.get_measurer()['is_valid']:
                    sigmas.append(log.get_stats().get_sigma_perc())

            prev_len_sigmas = len(sigmas)

            if config.getboolean("filters", "discard_outliers"):
                outliers_k = float(config["filters"]["outliers_multiplier"])
                sigmas = remove_outliers(sigmas, outliers_k)

            current_production_length = len(sigmas)

            for sigma in sigmas:
                sigma_sigmas += math.pow((sigma - np.mean(sigmas)) / np.mean(sigmas) * 100, 2)
                if sigma < np.mean(sigmas):
                    sigma_counter += 1

            if current_production_length > 0:

                sigma_sigmas = math.sqrt(sigma_sigmas / current_production_length)

                log_index = log_for_actuation_stats
                if current_production_length <= log_for_actuation_stats:
                    log_index = current_production_length - 1

                # Add production to csv
                writer.writerow({
                    'date': str(production.get_start_date()).split(" ")[0].replace("-", "/"),
                    'time': str(production.get_start_date()).split(" ")[1].replace(".", ","),
                    'cause': str(production.get_trigger()),
                    'actuation started flat': str(production.started_flat()),
                    'actuation active': str(production.get_logs()[0].get_actuation().is_active()),
                    'profiles': str(round(current_production_length, 2)).replace(".", ","),
                    'outliers': str(round(prev_len_sigmas - len(sigmas), 2)).replace(".", ","),
                    'total length': str(round(len(production.get_logs()), 2)).replace(".", ","),
                    'outliers percentage': str(
                        round(((prev_len_sigmas - len(sigmas)) / len(production.get_logs())) * 100,
                              2)).replace(".", ","),
                    'zones': str(len(production.get_logs()[0].get_actuation().get_actuators())),
                    'layflat': str(round(production.get_layflat_avg(), 2)).replace(".", ","),
                    'flowrate': str(round(production.get_flowrate_avg(), 2)).replace(".", ","),
                    'BUR': str(round(production.compute_bur(production.get_layflat_avg(),
                                                            float(config["details"]["die_diameter"])), 2)).replace(".",
                                                                                                                   ","),
                    'speed m/min': str(round(production.get_speed_avg(), 2)).replace(".", ","),
                    'thickness um': str(round(production.get_thickness_avg(), 2)).replace(".", ","),
                    '2sigma start': str(round(production.get_sigma_start(), 2)).replace(".", ","),
                    '2sigma mean': str(round(np.mean(sigmas), 2)).replace(".", ","),
                    '2sigma best': str(round(np.min(sigmas), 2)).replace(".", ","),
                    '2sigma worst': str(round(np.max(sigmas), 2)).replace(".", ","),
                    '2sigmaW - 2sigmaB': str(round(np.max(sigmas) - np.min(sigmas), 2)).replace(".", ","),
                    '2sigma improvement':
                        str(round((1 - (np.mean(sigmas) / production.get_sigma_start())) * 100, 0)).replace(".", ","),
                    'time to halve 2sigma': str(production.get_time_to_halve_sigma()).replace(".", ","),
                    sigma_improvement_after_string: str(round(100 - (production.get_sigma_after(log_index) /
                                                                     production.get_sigma_start() * 100), 2)).replace(
                        ".", ","),
                    '2sigma under average': str(
                        round((sigma_counter / current_production_length) * 100, 2)).replace(".", ","),
                    '2sigma sigma': str(round(sigma_sigmas, 2)).replace(".", ","),
                    actuation_mean_string:
                        str(round(production.get_logs()[log_index].get_stats().get_actuators_mean())).
                            replace(".", ","),
                    actuation_std_dev_string:
                        str(round(production.get_logs()[log_index].get_stats().get_actuators_std_dev())).
                            replace(".", ","),
                    zeroed_actuators_string: str(round(np.count_nonzero(np.array(production.get_logs()[log_index].
                                                                                 get_actuation().actuators) == 0))).replace(
                        ".", ",")
                })
            else:
                # Add production to csv
                writer.writerow({'date': str(production.get_start_date()).split(" ")[0].replace("-", "/"),
                                 'time': str(production.get_start_date()).split(" ")[1].replace(".", ","),
                                 'cause': str(production.get_trigger()),
                                 'actuation started flat': str(False),
                                 'actuation active': str(production.get_logs()[0].get_actuation().is_active()),
                                 'profiles': str(round(current_production_length, 2)).replace(".", ","),
                                 'outliers': str(round(prev_len_sigmas - len(sigmas), 2)).replace(".",
                                                                                                  ","),
                                 'total length': str(round(len(production.get_logs()), 2)).replace(".",
                                                                                                   ","),
                                 'outliers percentage': str(
                                     round(((prev_len_sigmas - len(sigmas)) / len(production.get_logs())) * 100,
                                           2)).replace(".", ","),
                                 'zones': str(len(production.get_logs()[0].get_actuation().get_actuators())),
                                 'layflat': str(round(production.get_layflat_avg(), 2)).replace(".", ","),
                                 'flowrate': str(round(production.get_flowrate_avg(), 2)).replace(".", ","),
                                 'BUR': str(round(production.compute_bur(production.get_layflat_avg(),
                                                                         float(config["details"]["die_diameter"])),
                                                  2)).replace(
                                     ".", ","),
                                 'speed m/min': str(round(production.get_speed_avg(), 2)).replace(".", ","),
                                 'thickness um': str(round(production.get_thickness_avg(), 2)).replace(".", ","),
                                 '2sigma start': str(round(production.get_sigma_start(), 2)).replace(".", ","),
                                 '2sigma mean': str(0).replace(".", ","),
                                 '2sigma best': str(0).replace(".", ","),
                                 '2sigma worst': str(0).replace(".", ","),
                                 '2sigmaW - 2sigmaB': str(0).replace(".", ","),
                                 '2sigma improvement': str(0).replace(".", ","),
                                 'time to halve 2sigma': str(0).replace(".", ","),
                                 sigma_improvement_after_string: str(0).replace(".", ","),
                                 '2sigma under average': str(0).replace(".", ","),
                                 '2sigma sigma': str(0).replace(".", ","),
                                 actuation_mean_string: str(0).replace(".", ","),
                                 actuation_std_dev_string: str(0).replace(".", ","),
                                 zeroed_actuators_string: str(0).replace(".", ",")
                                 })


def compute_actuators_incidence(productions: List[Production]):
    for production in productions:
        actuation_variance_list = []
        for log in production.get_logs():
            actuation_variance_list.append(log.get_stats().get_actuators_std_dev())
        plt.plot(remove_outliers(actuation_variance_list, 1.5))
        plt.xlabel("profile")
        plt.ylabel("variance")
        plt.show()


# Print iterations progress
def print_progress_bar(iteration, total, prefix='', suffix='', decimals=1, length=100, fill='█', print_end="\r"):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
        printEnd    - Optional  : end character (e.g. "\r", "\r\n") (Str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filled_length = int(length * iteration // total)
    bar = fill * filled_length + '-' * (length - filled_length)
    print(f'\r{prefix} |{bar}| {percent}% {suffix}', end=print_end)
    # Print New Line on Complete
    if iteration == total:
        print()
